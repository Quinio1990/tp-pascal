program tp3;

uses crt;

const
        dir_jugadores='jugadores.csv';
        dir_jugador='jugador.dat';
        dir_equiposcsv='equipos.csv';
        dir_participantescsv='participantes.csv';
        dir_participantes='participantes.dat';
        dir_equipos='equipos.dat';
        maxjugadores=623 ;
        MAXJUGEQUI=15;
        MAXPRES=56000000;
        MAXJUGMISMOEQUIPO=3;
type
        tPosicion = (ARQUERO, DEFENSOR, MEDIOCAMPISTA, DELANTERO);


        tvtactica=array[1..5] of string;


        tjugador=record
                id:longint;
                EQUIPO_REAL:string;
                apeynom:string;
                pos:string; {este campo esta hecho para tratar mas facil el archivo de texto}
                posicion:tPosicion;
                precio:real;
        end;

        tajugador=file of tjugador;

        trindex=record
                club:string;
                pos:integer;
        end;




        tvindex=array [1..maxjugadores] of trindex;

        trparticipantes=record
                idpart:integer;
                nom:string;
                apellido:string;
                hincha:string;
                usuario:string[10];
                pass:string;
                idequipo:integer;
                admin:boolean;
                end;

        taparticipantes= file of trparticipantes;

        tvjugadores=array[1..MAXJUGEQUI] of tjugador;


        trequipos=record
                idequipo:integer;
                jugadores:tvjugadores;
                end;

        taequipos=file of trequipos;

        Trequipo=record
                 jugadores:tvjugadores;
                 presupuestototal:real;
                 end;

{******************************* VALIDACIONES DE LA UNIT ANTERIOR CON MODIFICACIONES ****************************************}


function Validarjugequipo(var jugador:tjugador;var equipo:trequipo):boolean;


var
    i,j,cant:byte;
    max:boolean;

begin
        cant:=0;
        i:=1;
        max:=false;
        while (i<=MAXJUGEQUI) and (not max) do
        begin
                if equipo.jugadores[i].equipo_real=jugador.equipo_real then
                begin
                        if cant<>3 then
                        begin
                                inc(cant);
                        end;
                        if cant=3 then
                        begin;
                                max:=true;
                        end;
                end;
                inc(i);
        end;
        validarjugequipo:=(not max);


end;

function validarjugid(var jugador:tjugador; var equipo:trequipo):boolean;

var     i,cont:byte;
        b:boolean;


begin
        i:=1;
        cont:=1;
        b:=true;
        while (i<=MAXJUGEQUI) and (b) do
        begin
                if jugador.id=equipo.jugadores[i].id then
                begin
                        inc(cont);
                        if cont>1 then
                                b:=false;
                end;

                inc(i);
        end;
        validarjugid:=b;
end;


procedure contarjugposicion(var equipo:trequipo;var arq,def,med,del:byte);

var

         i:byte;

begin
        arq:=0;
        def:=0;
        med:=0;
        del:=0;
        i:=1;
        while  (i<=MAXJUGEQUI) and (equipo.jugadores[i].id<>0) do
        begin
                case equipo.jugadores[i].posicion of
                                ARQUERO:inc(arq);
                                DEFENSOR:inc(def);
                                MEDIOCAMPISTA:inc(med);
                                DELANTERO:inc(del);
                end;
                inc(i);

        end;
end;


function validarjugpresupuesto(jugador:tjugador;var equipo:trequipo):boolean;

begin

  equipo.presupuestototal:=equipo.presupuestototal+jugador.precio;

  if (equipo.presupuestototal<=MAXPRES) then

        Validarjugpresupuesto:=true

   else
   begin
        equipo.presupuestototal:=equipo.presupuestototal-jugador.precio;
        Validarjugpresupuesto:=false;
   end;

end;

function validarjugposicion(var jugador:tjugador;var equipo:trequipo):boolean;

var     arq,def,med,del:byte;
        b:boolean; //Este booleano reemplazaria a la funcion validarjuposicion, siendo verdadera en caso de que el jugador no rompa la regla.

begin
        b:=false;
        contarjugposicion(equipo,arq,def,med,del);
        case jugador.posicion of
                ARQUERO:
                        begin
                               if arq<2 then
                                        b:=true;
                        end;
                DEFENSOR:
                        begin
                                case def of
                                        0..3:b:=true;
                                        4:
                                        begin
                                                case med of
                                                        0..4:b:=true;
                                                        5:
                                                        begin
                                                                if (del<=3) then
                                                                        b:=true;
                                                        end;
                                                        6:
                                                        begin
                                                                if (del<=2) then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
                MEDIOCAMPISTA:
                        begin
                                case med of
                                        0..3:b:=true;
                                        4:
                                        begin
                                                case def of
                                                        0..4:b:=true;
                                                        5:
                                                        begin
                                                                if del<4 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                        5:
                                        begin
                                                case def of
                                                        0..3:b:=true;
                                                        4:
                                                        begin
                                                                if del<4 then
                                                                        b:=true;
                                                        end;
                                                        5:
                                                        begin
                                                                if del<3 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
                DELANTERO:
                        begin
                                case del of
                                        0,1:b:=true;
                                        2:
                                        begin
                                                case def of
                                                        0..4:b:=true;
                                                        5:
                                                        begin
                                                                if med<6 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                        3:
                                        begin
                                                case def of
                                                        0..3:b:=true;
                                                        4:
                                                        begin
                                                                if med<6 then
                                                                        b:=true;
                                                        end;
                                                        5:
                                                        begin
                                                                if med<5 then
                                                                        b:=true;
                                                        end;
                                                end;
                                        end;
                                end;
                        end;
        end;
        validarjugposicion:=b;
end;



function validarjugador (jugador:tjugador;var equipo:trequipo):boolean;

var a,val:boolean;

begin
       if validarjugid(jugador,equipo) then
        begin
                if validarjugposicion(jugador,equipo) then
                begin
                        if validarjugequipo(jugador,equipo) then
                        begin
                                if validarjugpresupuesto(jugador,equipo) then

                                        val:=true


                                else

                                        val:=false;


                        end
                        else

                                val:=false;


                end
                else

                        val:=false;


        end
        else


                val:=false;


        validarjugador:=val;
end;


{******************************************************* FIN VALIDACIONES UNIT ANTERIOR ******************************}

procedure ObtenerRegJugador (id:integer; var jugador:tjugador;var ajugador:tajugador);



begin

                seek(Ajugador,(id-1));
                read(Ajugador,jugador);

end;

procedure iniciarunequipo (var unequipo:trequipo);

var i:byte;
begin

for i:=1 to MAXJUGEQUI do
                unequipo.jugadores[i].id:=0;

end;


function Equipovalido(var Aequipo:TAequipos; id:byte; var jugador:Tjugador; var equipo:Trequipos;var ajugador:tajugador):boolean;
var
        valido:boolean;
        i:byte;
        unequipo:trequipo;

begin

        seek(Aequipo,ID);
        read(Aequipo,Equipo);
        iniciarunequipo(unequipo);

        i:=1;
        valido:=true;

        while (i<=MAXJUGEQUI) and (valido) do
        begin
                obtenerRegJugador(equipo.jugadores[i].id,jugador,ajugador);


                if validarjugador(jugador,unequipo) then
                        valido:=true
                else
                        valido:=false;

                if valido then
                        unequipo.jugadores[i]:=jugador;


                inc(i);


        end;

        equipovalido:=valido;
end;




procedure procesarjugador(var cadena:string;var contador:byte;var jugador:tjugador;var num,cod:longint);


begin
        case contador of
                1:begin

                        val(cadena,num,cod);
                        jugador.id:=num;

                  end;
                2:jugador.EQUIPO_REAL:=upcase(cadena);
                3:jugador.apeynom:=cadena;
                4:begin
                        if cadena='ARQUERO' then
                                jugador.posicion:=ARQUERO
                        else
                        if cadena='DEFENSOR' then
                                jugador.posicion:=DEFENSOR
                        else
                        if cadena='MEDIOCAMPISTA' then
                                jugador.posicion:=MEDIOCAMPISTA
                        else
                                jugador.posicion:=DELANTERO;
                  end;


        end;


end;


procedure importarjugadores(var ajugadores:text;var ajugador:tajugador);

var
        c:char;
        cadena:string;
        contador:byte;
        jugador:tjugador;
        num,cod:longint;

begin
        contador:=1;
        cadena:='';
        while (not eof(ajugadores)) do
        begin

                while (not eoln(ajugadores)) do
                begin
                        read(ajugadores,c);
                        if (c<>'#')  then
                        begin

                               cadena:=cadena+c;
                               if contador=5 then
                               begin
                                        val(cadena,num,cod);
                                        jugador.precio:=num;
                               end;
                        end
                        else
                        begin
                               procesarjugador(cadena,contador,jugador,num,cod);
                               cadena:='';
                               inc(contador);

                        end;

                end;

                write(ajugador,jugador);
                readln(ajugadores);
                contador:=1;
                cadena:='';

        end;

end;

procedure indexar(var vindex:tvindex;var ajugador:tajugador);

var
      i,j,pos:integer;
      rindex,aux:trindex;
      jugador:tjugador;
begin

        reset(ajugador);
        pos:=0;
        while not eof(ajugador) do
        begin
                read(ajugador,jugador);
                rindex.club:=jugador.EQUIPO_REAl;
                rindex.pos:=pos;
                vindex[(pos+1)]:=rindex;
                inc(pos);
        end;

        for i:=1 to (maxjugadores-1) do
                for j:=i+1 to (maxjugadores) do
                begin
                        if vindex[i].club>vindex[j].club then
                          begin
                                aux:=vindex[i];
                                vindex[i]:=vindex[j];
                                vindex[j]:=aux;
                          end;
                end;


end;

procedure mostrarequipo(var vindex:tvindex;var ajugador:tajugador);

var
        eq:string;  rjugador:tjugador; pos:integer; enc:boolean; opc:char;

begin
     repeat
        clrscr;
        writeln('Ingrese el nombre del equipo');
        readln(eq);
        eq:=upcase(eq);

        pos:=1;
        enc:=false;

        while (pos<=maxjugadores) and (not enc) do
        begin
                if (vindex[pos].club=eq) then
                        while (pos<=maxjugadores) and  (vindex[pos].club=eq)  do
                        begin
                                seek(ajugador,vindex[pos].pos);
                                read(ajugador,rjugador);
                                writeln(rjugador.id,'  ',rjugador.apeynom,'  ',rjugador.precio:6:0);
                                enc:=true;
                                inc(pos);
                        end;


                inc(pos);
        end;

        if (not enc) then
                writeln('No se encuentra el equipo');

        writeln('�Desea ver otro equipo?');
        opc:=readkey;

     until upcase(opc)='N';


end;

procedure procesarparticipantes(var cadena:string;var contador:byte;var rparticipantes:trparticipantes);

var
     num,cod:integer;

begin
        case contador of
                1:begin
                        val(cadena,num,cod);
                        rparticipantes.idpart:=num;
                  end;
                2: rparticipantes.nom:=cadena;
                3: rparticipantes.apellido:=cadena;
                4: rparticipantes.hincha:=cadena;
                5: rparticipantes.usuario:=cadena;
                6: rparticipantes.pass:=cadena;
                7: begin
                        val(cadena,num,cod);
                        rparticipantes.idequipo:=num;
                   end;
                8: begin
                        val(cadena,num,cod);
                        if num=0 then
                                rparticipantes.admin:=true
                        else if num=1 then
                                rparticipantes.admin:=false
                   end;
         end;
end;


procedure importarparticipantes(var aparticipantes:taparticipantes;var aparticipantescsv:text);


var
        contador:byte; c:char; cadena:string;   rparticipantes:trparticipantes;

begin
        contador:=1;
        rewrite(aparticipantes);
        cadena:='';
        while (not eof(aparticipantescsv)) do
        begin

                while (not eoln(aparticipantescsv)) do
                begin
                        read(aparticipantescsv,c);
                        if (c<>'#') and (not eoln(aparticipantescsv)) then
                                 cadena:=cadena+c
                        else
                        begin
                                procesarparticipantes(cadena,contador,rparticipantes);
                                cadena:='';
                                inc(contador);
                        end;
                end;

                write(aparticipantes,rparticipantes);
                readln(aparticipantescsv);
                contador:=1;
                cadena:='';
        end;

end;

procedure procesarequipos(var cadena:string; var contador:byte; var requipos:trequipos);

var
        num,cod:integer;
begin
        val(cadena,num,cod);
        if cod=0 then
        begin
                case contador of
                        1:requipos.idequipo:=num;
                        2..16:begin
                                if num<=MAXJUGADORES then
                                        requipos.jugadores[contador-1].id:=num;
                              end;
                end;
        end;
end;

procedure importarequipos(var aequipos:taequipos;var aequiposcsv:text);

var
        contador:byte; cadena:string; c:char; requipos:trequipos;

begin
        rewrite(aequipos);
        contador:=1;
        cadena:='';

        while (not eof(aequiposcsv)) do
        begin
                while (not eoln(aequiposcsv)) do
                begin
                        read(aequiposcsv,c);
                        if (c<>'#') then
                                cadena:=cadena+c
                        else
                        begin
                                procesarequipos(cadena,contador,requipos);
                                cadena:='';
                                inc(contador);
                        end;
                end;

                write(aequipos,requipos);
                readln(aequiposcsv);
                cadena:='';
                contador:=1;
        end;
end;

procedure ingresarpass(var contrasenia:string);

 var
       caracter:char; cont:byte;
 begin
  cont:=0;
  contrasenia:='';

  repeat

   caracter:=readkey;

    if caracter<>#13 then
     begin
      write('*');
      contrasenia:=contrasenia+caracter;
      inc(cont);
     end;

  until (cont=10) or (caracter=#13);

 end;


procedure loginparticipante (var aparticipantes:taparticipantes;var usuario:string;var pass:string;
                              var rparticipantes:trparticipantes;var enc:boolean);


begin

        reset(aparticipantes);
        enc:=false;
        while (not eof(aparticipantes)) and (not enc) do
        begin
                read(aparticipantes,rparticipantes);
                if (rparticipantes.usuario=usuario) and (rparticipantes.pass=pass) then
                        enc:=true
        end;

        if enc=false then
        begin
                        clrscr;
                        writeln('Usuario y/o Contrase�a invalida');
                        readkey;
        end;


end;






function existeequipo(var idequipo:integer; var aequipos:taequipos):boolean;

var
        requipo:trequipos;

begin
        seek(aequipos,idequipo);
        {$i-}
                read(aequipos,requipo);
        {$i+}
        if ioresult<>0 then
                existeequipo:=false
        else
                existeequipo:=true;

end;

procedure crearparticipante(var aparticipantes:taparticipantes);

var    participante:trparticipantes;
       auxiliarparticipante:trparticipantes;
       contrasenia:string[10];
       igual:boolean;

begin
    clrscr;
    reset(aparticipantes);
    writeln('Ingrese su nombre');
    readln(participante.nom);
    writeln('Ingrese su apellido');
    readln(participante.apellido);
    writeln('Ingrese el club del que es hincha');
    readln(participante.hincha);
    writeln('Ingrese su nombre de Usuario');
    readln(participante.usuario);
    while (not eof(aparticipantes)) do
        begin
                read(aparticipantes,auxiliarparticipante);
                if (auxiliarparticipante.usuario=participante.usuario) then
		begin
			writeln('El nombre ya esta utilizado, ingrese nuevamente');
			readln(participante.usuario);
			reset(aparticipantes);
		end; //aca solo entre cuando el nombre es igual, sino no entra}
        end; //y cuando sale del while, esta al final del archivo, y con el ultimo id guardado
    participante.idpart:=auxiliarparticipante.idpart+1;
    repeat
        writeln('Ingrese su contrase�a');
        ingresarpass(contrasenia);
        writeln('Ingrese nuevamente');
        ingresarpass(participante.pass);
    until (participante.pass=contrasenia);
    write(aparticipantes,participante);
    writeln('El usuario ha sido creado');
end;

procedure menu1(var aparticipantes:taparticipantes; var aequipos:taequipos;var ajugador:tajugador);

var
        usuario,pass:string; rparticipantes:trparticipantes;
        logueado,enc:boolean; opc,opc1:char;
        id:integer;
        jugador:tjugador;
        equipo:trequipos;
begin

      logueado:=false;

      repeat
        clrscr;
        writeln('1. Crear Usuario');
        writeln;
        writeln('2. Login');
        writeln;
        writeln('3. Men� Anterior');

        opc:=readkey;

        case opc of
        '1':crearparticipante(aparticipantes);
        '2': begin
                clrscr;
                writeln('Ingrese Usuario');
                readln(usuario);
                writeln('Ingrese Contrase�a');
                ingresarpass(pass);
                loginparticipante(aparticipantes,usuario,pass,rparticipantes,enc);
                if enc then
                begin
                        logueado:=true;
                        if existeequipo(rparticipantes.idequipo,aequipos) then
                        begin

                                if Equipovalido(Aequipos,rparticipantes.idequipo,jugador,equipo,ajugador) then
                                begin
                                        clrscr;
                                        writeln('El equipo cumple con todas las condiciones');
                                        readkey;
                                end
                                else
                                begin
                                        clrscr;
                                        writeln('El equipo no cumple todas las condiciones');
                                        readkey;
                                end;
                        end
                        else
                        begin
                                clrscr;
                                writeln('No existe equipo');
                                readkey;
                        end;

                        while logueado do
                             begin
                                clrscr;
                                writeln('1.Logout');
                                opc1:=readkey;
                                if opc1='1' then
                                        logueado:=false;
                             end;
                end;
              end;
          end;

      until opc='3';

end;

procedure menuprincipal(var aparticipantes:taparticipantes; var aequipos:taequipos;
                        var vindex:tvindex;var ajugador:tajugador);

var
        opc:char;

begin

      repeat

        clrscr;
        writeln('1. Crear o Verificar Usuarios');
        writeln;
        writeln('2. Indexar');
        writeln;
        writeln('3. Salir');

        opc:=readkey;

        case opc of
                '1':menu1(aparticipantes,aequipos,ajugador);
                '2':begin
                        indexar(vindex,ajugador);
                        mostrarequipo(vindex,ajugador);
                    end;
         end;

      until opc='3';

end;



var
        ajugadores:text;
        ajugador:tajugador;
        jugador:tjugador;
        vindex:tvindex;
        aparticipantescsv:text;
        aparticipantes:taparticipantes;           rparticipantes:trparticipantes;
        aequipos:taequipos;
        aequiposcsv:text;

 begin
        assign(ajugadores,dir_jugadores);
        assign(ajugador,dir_jugador);
        assign(aparticipantescsv,dir_participantescsv);
        assign(aparticipantes,dir_participantes);
        assign(aequiposcsv,dir_equiposcsv);
        assign(aequipos,dir_equipos);
        rewrite(ajugador);
        reset(ajugadores);
        reset(aparticipantescsv);
        reset(aequiposcsv);
        importarjugadores(ajugadores,ajugador);
        importarparticipantes(aparticipantes,aparticipantescsv);
        importarequipos(aequipos,aequiposcsv);
        menuprincipal(aparticipantes,aequipos,vindex,ajugador);
        close(ajugador);
        close(ajugadores);
        close(aparticipantescsv);
        close(aparticipantes);
        close(aequipos);
        close(aequiposcsv);
end.

